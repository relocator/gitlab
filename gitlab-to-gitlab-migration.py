from urllib2 import urlopen
import json
import subprocess, shlex
import os
import argparse, sys

parser=argparse.ArgumentParser()

parser.add_argument('--oldserver', help='Defines the base URL of the old gitlab server, e.g. git.yourdomain.com')
parser.add_argument('--token1', help='Defines the old gitlab server\'s API token')
parser.add_argument('--newserver', help='Defines the base URL of the new gitlab server')
parser.add_argument('--newnamespace', help='Defines the new gitlab namespace, i.e. group. For example "customer1/projectA"')
parser.add_argument('--token2', help='Defines the new gitlab server\'s API token.')

args=parser.parse_args()


allProjects     = urlopen("https://{}/api/v4/projects?private_token={}&page=1&per_page=1000&membership=yes".format(args.oldserver, args.token1))
allProjectsDict = json.loads(allProjects.read().decode())
for thisProject in allProjectsDict: 
    try:
        thisProjectURL  = thisProject['http_url_to_repo']
        thisProjectName = thisProject['name']
        dirName = thisProjectName
       
        cloneCommand     = shlex.split('git clone --mirror %s' % thisProjectURL)
        resultCodeClone  = subprocess.Popen(cloneCommand)
        resultCodeClone.wait()
    
        fullPath = os.getcwd() + "/" + dirName + ".git"

        neworigin = "https://oauth2:{}@{}/{}/{}.git".format(args.token2, args.newserver, args.newnamespace, dirName)
        mirrorCommand = '''
        cd {} 
        git push --mirror {}'''.format(fullPath, neworigin) 

        mirrorResult = subprocess.check_output(mirrorCommand, shell=True)
        mirrorResult.wait()

    except Exception as e:
        print(e)