# Gitlab to Gitlab Migration Tool

This project allows you to relocate all projects you are **a member of** (Master, Owner, etc.) on a given GitLab instance 
to another GitLab instance. The tool is using "git push --mirror" which means that all your branches and tags will be migrated as well. 

You need to create two personal access tokens, one on the old instance and one on the new one. 
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

1) Token 1 (old server): configure the token with the following scopes : **read_registry** (if it is a private registry), **read_repository**
2) Token 2 (new server): configure the token with the **write_repository** scope

You have to provide the namespace on the new server as a command-line parameter. 


Run the script like this:

```
$ python gitlab-to-gitlab-migration.py 
    --oldserver git.olddomain.com
    --token1 TOKEN_OLD_SERVER
    --newserver git.newdomain.com
    --token2 TOKEN_NEW_SERVER 
    --newnamespace group_name/subgroup_name
```